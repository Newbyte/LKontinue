#!/usr/bin/env python

from urllib.parse import urlparse
from urllib.request import urlopen
import requests
from bs4 import BeautifulSoup

unblockPage = "http://steampowered.com"
url = requests.get(unblockPage).url

if url == "https://store.steampowered.com/":
    exit()

page = urlopen(unblockPage)

toParseUrl = urlparse(url)
hostname = '{uri.scheme}://{uri.netloc}/'.format(uri=toParseUrl)
html = BeautifulSoup(page, "html.parser")
output = html.form["action"]
token = html.input["name"]

postUrl = hostname[0:-1] + output

postData = {
        token: "Continue"
    }

print(token)
print(postUrl)

requests.post(url = postUrl, data = postData)

